package com.gariol.cazalibros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.gariol.cazalibros.Models.Book;
import com.google.gson.Gson;

public class BookDetailActivity extends AppCompatActivity {

    public static final String ARG_LOADED_BOOK = "ARG_LOADED_BOOK";
    private Book currentBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        loadCurrentBook();
    }

    private void loadCurrentBook() {
        Intent intent = getIntent();
        String stringBook = intent.getStringExtra(ARG_LOADED_BOOK);
        Gson gson = new Gson();
        currentBook = gson.fromJson(stringBook, Book.class);
        initViews();
    }

    private void initViews() {
        TextView textViewBookTitle = (TextView)findViewById(R.id.textView_bookTitle);
        TextView textViewBookAuthor = (TextView)findViewById(R.id.textView_bookAuthor);
        TextView textViewBookPages = (TextView)findViewById(R.id.textView_bookPages);
        TextView textViewBookDescription = (TextView)findViewById(R.id.textView_bookDescription);
        TextView textViewBookGenre = (TextView)findViewById(R.id.textView_bookGenre);

        textViewBookTitle.setText(currentBook.getName());
        textViewBookAuthor.setText(currentBook.getAuthor());
        textViewBookPages.setText(currentBook.getPages());
        textViewBookDescription.setText(currentBook.getDescription());
        textViewBookGenre.setText(currentBook.getGenre());
    }
}
